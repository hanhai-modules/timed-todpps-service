/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <hanhai/infra/nlohmann/json.hpp>
#include "timesync_msgbox_log_server_wrap.h"
#include <signal.h>
#include <thread>
#include <iostream>
// #include "bstipc_cfg.h"

using Json = nlohmann::json;

static bool stop_flag = false;

static void on_log_received(const uint32_t domain_id, const uint32_t status, const char *log_msg)
{
    if (domain_id == 0)
        std::cout << "SW Log: " << log_msg << std::endl;
    else if (domain_id == 2)
        std::cout << "RT Log: " << log_msg << std::endl;
}

static void SignalHandler(int signum)
{
    stop_flag = true;
}

int main(int argc, char *argv[])
{
    // uint8_t pid = CPU_5;
    // uint8_t fid = DEF;
    // uint8_t sid = 5U;
    if (!timesync_msgbox_log_server_init(0, 0, 0))
        return -1;
    
    timesync_msgbox_server_reg_log_func(&on_log_received);

    signal(SIGINT, SignalHandler);
    signal(SIGILL, SignalHandler);
    signal(SIGABRT, SignalHandler);
    signal(SIGSEGV, SignalHandler);
    signal(SIGTERM, SignalHandler);
    signal(SIGBUS, SignalHandler);

    while (!stop_flag)
        std::this_thread::sleep_for(std::chrono::milliseconds(1));

    timesync_msgbox_log_server_destroy();
    return 0;
}
