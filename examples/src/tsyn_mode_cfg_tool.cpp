/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <hanhai/infra/nlohmann/json.hpp>
#include "timesync_msgbox_client_wrap.h"
#include <string>
#include <signal.h>
#include <fstream>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
// #include "bstipc_cfg.h"

#define GTC_IOC_MAGIC 'g'
#define GTC_IOC_LATCH_CFG _IOW(GTC_IOC_MAGIC, 1, int)
#define GTC_IOC_INTR_CFG _IOW(GTC_IOC_MAGIC, 2, int)
#define GTC_IOC_MUX_CFG _IOW(GTC_IOC_MAGIC, 3, int)
#define GTC_IOC_GET_PARM _IOR(GTC_IOC_MAGIC, 4, struct time_sync_parm)
#define GTC_IOC_GET_FREQ _IOR(GTC_IOC_MAGIC, 5, struct gtc_freq)

#define SAFETY_LATCH_MUX 4
#define ADAS_LATCH_MUX 8
#define SWITCH_LATCH_MUX 12
#define REALTIME_LATCH_MUX 16

using Json = nlohmann::json;

static TimeSyncConfig g_sw_cfg_info = {0};
static TimeSyncConfig g_rt_cfg_info = {0};
static bool g_sw_cfg = false;
static bool g_rt_cfg = false;

static void SignalHandler(int32_t signum)
{
    timesync_msgbox_client_destroy();
    exit(0);
}

static bool Init_Config(const std::string &config_file)
{
    // 打开配置文件
    std::ifstream config_stream(config_file);
    if (!config_stream.good())
    {
        std::cout << "Error opening config file: " << config_file << std::endl;
        return false;
    }

    // 解析为json对象
    Json config{};
    config_stream >> config;
    config_stream.close();

    // 解析sw_config
    if (!config["sw_config"].is_null())
    {
        g_sw_cfg = true;
        Json sw_config = config["sw_config"];
        // 解析todpps_mode配置项，该项为可选项
        if (!sw_config["todpps_mode"].is_null())
            g_sw_cfg_info.todpps_mode = sw_config["todpps_mode"];
        // 解析cansync_mode配置项，该项为可选项
        if (!sw_config["cansync_mode"].is_null())
            g_sw_cfg_info.cansync_mode = sw_config["cansync_mode"];
        // 解析gptp_mode配置项，该项为可选项
        if (!sw_config["gptp_mode"].is_null())
            g_sw_cfg_info.gptp_mode = sw_config["gptp_mode"];
        // 解析ptp_mode配置项，该项为可选项
        if (!sw_config["ptp_mode"].is_null())
            g_sw_cfg_info.ptp_mode = sw_config["ptp_mode"];
    }

    // 解析rt_config
    if (!config["rt_config"].is_null())
    {
        g_rt_cfg = true;
        Json rt_config = config["rt_config"];
        // 解析todpps_mode配置项，该项为可选项
        if (!rt_config["todpps_mode"].is_null())
            g_rt_cfg_info.todpps_mode = rt_config["todpps_mode"];
        // 解析cansync_mode配置项，该项为可选项
        if (!rt_config["cansync_mode"].is_null())
            g_rt_cfg_info.cansync_mode = rt_config["cansync_mode"];
        // 解析gptp_mode配置项，该项为可选项
        if (!rt_config["gptp_mode"].is_null())
            g_rt_cfg_info.gptp_mode = rt_config["gptp_mode"];
        // 解析ptp_mode配置项，该项为可选项
        if (!rt_config["ptp_mode"].is_null())
            g_rt_cfg_info.ptp_mode = rt_config["ptp_mode"];
    }

    return true;
}

static bool GtcEnable(uint32_t latch, uint32_t inter, uint32_t mux)
{
    int32_t fd = open("/dev/gtc", O_RDWR);
    if (fd < 0)
    {
        std::cout << "Error opening gtc" << std::endl;
        return false;
    }
    if (ioctl(fd, GTC_IOC_LATCH_CFG, &latch))
    {
        std::cout << "Error ioctl GTC_IOC_LATCH_CFG" << std::endl;
        close(fd);
        return false;
    }
    if (ioctl(fd, GTC_IOC_INTR_CFG, &inter))
    {
        std::cout << "Error ioctl GTC_IOC_INTR_CFG" << std::endl;
        close(fd);
        return false;
    }
    if (ioctl(fd, GTC_IOC_MUX_CFG, &mux))
    {
        std::cout << "Error ioctl GTC_IOC_MUX_CFG" << std::endl;
        close(fd);
        return false;
    }
    close(fd);
    return true;
}

int main(int argc, char *argv[])
{
    // uint8_t pid = CPU_5;
    // uint8_t fid = DEF;
    // uint8_t sid = 8U;
    uint32_t ret = 1;
    std::string config_file{"/usr/bin/examples/timesync/conf/tsyn-mode-cfg.json"};
    if (!Init_Config(config_file))
        return -1;

    signal(SIGINT, SignalHandler);
    signal(SIGILL, SignalHandler);
    signal(SIGABRT, SignalHandler);
    signal(SIGSEGV, SignalHandler);
    signal(SIGTERM, SignalHandler);
    signal(SIGBUS, SignalHandler);

    // if (!timesync_msgbox_client_init(CPU_5, DEF, 8U))
    if (!timesync_msgbox_client_init(0, 0, 0))
        return -1;

    if (g_rt_cfg)
    {
        std::cout << "Send realtime config info by msgbox!" << std::endl;
        if (g_rt_cfg_info.todpps_mode == 1 && !GtcEnable(REALTIME_LATCH_MUX, 1, REALTIME_LATCH_MUX))
            std::cout << "Error configuring gtc for realtime" << std::endl;
        if (!timesync_msgbox_client_send_mode_info(REALTIME, &g_rt_cfg_info, &ret))
            std::cout << "Send config info to realtime fail" << std::endl;
        else if (ret != 0)
            std::cout << "Configure realtime fail, ret = " << ret << std::endl;
        else
            std::cout << "Configure realtime success!" << std::endl;
    }

    if (g_sw_cfg)
    {
        std::cout << "Send switch config info by msgbox!" << std::endl;
        if ((g_sw_cfg_info.todpps_mode == 1 || g_sw_cfg_info.todpps_mode == 3)&& !GtcEnable(SWITCH_LATCH_MUX, 1, SWITCH_LATCH_MUX))
            std::cout << "Error configuring gtc for switch" << std::endl;
        if (!timesync_msgbox_client_send_mode_info(SWITCH, &g_sw_cfg_info, &ret))
            std::cout << "Send config info to switch fail" << std::endl;
        else if (ret != 0)
            std::cout << "Configure switch fail, ret = " << ret << std::endl;
        else
            std::cout << "Configure switch success!" << std::endl;
    }

    if ((g_rt_cfg_info.todpps_mode == 0 || g_rt_cfg_info.todpps_mode == 2) && 
        (g_sw_cfg_info.todpps_mode == 0 || g_sw_cfg_info.todpps_mode == 2))
    {
        if (!GtcEnable(ADAS_LATCH_MUX, 1, ADAS_LATCH_MUX))
            std::cout << "Error configuring gtc for adas" << std::endl;
    }

    timesync_msgbox_client_destroy();
    return 0;
}
