#[[
 *Copyright (c) 2024 Black Sesame Technologies
 *
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
]]

cmake_minimum_required(VERSION 3.10)
project(examples)
include(GNUInstallDirs)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
add_compile_options("-O2")

# coverage setting
if(ENABLE_COVERAGE)
    SET(CMAKE_BUILD_TYPE "Debug")
    SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fprofile-arcs -ftest-coverage")
    SET(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} -fprofile-arcs -ftest-coverage -lgcov")
endif()

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif()

add_executable(tsyn_mode_cfg_tool ./src/tsyn_mode_cfg_tool.cpp)
target_link_libraries(tsyn_mode_cfg_tool tsyn_msgbx_wrap)

add_executable(tsyn_log_server ./src/tsyn_log_server.cpp)
target_link_libraries(tsyn_log_server tsyn_msgbx_wrap)

install(TARGETS tsyn_mode_cfg_tool RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/examples/timesync)
install(TARGETS tsyn_log_server RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}/examples/timesync)

file(GLOB CONFIG_FILE
    ./conf/*.json
)
install(FILES ${CONFIG_FILE} DESTINATION ${CMAKE_INSTALL_BINDIR}/examples/timesync/conf)
