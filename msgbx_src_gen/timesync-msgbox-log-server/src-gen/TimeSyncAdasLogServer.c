// SPDX-License-Identifier: GPL-2.0 OR Apache 2.0
/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * This program is also distributed under the terms of the Apache 2.0
 * License.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This file is auto generated for message box v1.2.0.
 * All manual modifications will be LOST by next generation.
 * It is recommended NOT modify it.
 * Generator Version: francaidl 20bed9c msgbx_ipc e82b7ce
 */

#include "TimeSyncAdasLogServer.h"
#ifdef IPC_RTE_KERNEL
#include <bst/ipc_trans_common.h>
#include <bst/ipc_trans_layer.h>
#else
#include "ipc_trans_common.h"
#include "ipc_trans_layer.h"
#endif

#define PID CPU_5
#define FID DEF
#define SID 4U

static TimeSyncAdasLogServer_data_t *s_ins = NULL;

// receive messages
static int32_t receive_message(void)
{
	com_server_data_t *data = (com_server_data_t *)s_ins;

	if (!data)
		return -ERR_APP_PARAM;

	return ipc_trans_layer_query_info(data->pid, data->handle);
}

// dispatch messages
static int32_t dispatch_message(void)
{
	int32_t ret = 0;
	serdes_t *ser = NULL;
	serdes_t *des = NULL;
	com_server_data_t *data = (com_server_data_t *)s_ins;

	if (!data)
		return -ERR_APP_PARAM;
	ser = &data->serializer;
	des = &data->deserializer;

	while (ipc_trans_layer_stub_get_method_msg(data->pid, data->handle, des) >= 0) {
		bool need_reply = true;
	   ret = s_ins->server.adas_log_server.dispatch_request(des, &need_reply);
		if (ret < 0)
			IPC_LOG_ERR("adas_log_server dispatch request failed %d.\n", ret);

		if (need_reply) {
			(void)ipc_ser_init(ser);
			ret = ipc_ser_put_32(ser, (uint32_t *)&ret);
			ser->header = des->header;
			ser->header.cid = des->header.pid;
			ser->header.pid = data->pid;
			ser->header.typ = MSGBX_MSG_TYPE_REPLY;
			if (ret >= 0)
				ret = ipc_ser_finish(ser);
			if (ret >= 0)
				ret = send_reply(data, ser);
			if (ret < 0)
				IPC_LOG_ERR("send reply fail %d.\n", ret);
		}
	}
	return ret;
}
#ifndef IPC_RTE_BAREMETAL
#if defined IPC_RTE_KERNEL
static int router_func(void *arg)
#else
static void *router_func(void *arg)
#endif
{
	int32_t ret = 0;

#if defined IPC_RTE_POSIX
	while (s_ins && s_ins->com_data.bRunning) {
#elif defined IPC_RTE_KERNEL
	while (unlikely(!kthread_should_stop())) {
#endif
		ret = receive_message();
		if (ret != 0)
			continue;

		ret = dispatch_message();
		if (ret < 0)
			continue;
	}
#if defined IPC_RTE_KERNEL
	return RESULT_SUCCESS;
#else
	return arg;
#endif
}

// start message router
static int32_t start(void)
{
#if defined IPC_RTE_POSIX
	int32_t ret = 0;
#endif
	com_server_data_t *data = &s_ins->com_data;

	if (!data)
		return ERR_APP_PARAM;

	if (data->bRunning)
		return RESULT_SUCCESS;

	data->bRunning = true;
#if defined IPC_RTE_POSIX
	ret = pthread_create(&data->route_task, NULL, router_func, NULL);
	if (ret != 0) {
#elif defined IPC_RTE_KERNEL
	data->route_task = kthread_run(router_func, NULL, "TimeSyncAdasLogServer_thread");
	if (unlikely(!data->route_task)) {
#endif
		data->bRunning = false;
		return -ERR_APP_START;
	}

	return RESULT_SUCCESS;
}

// stop message router.
static int32_t stop(void)
{
	int32_t ret = 0;
	com_server_data_t *data = &s_ins->com_data;

	if (!data)
		return ERR_APP_PARAM;

	if (!data->bRunning)
		return RESULT_SUCCESS;

	//sleep 1 seconds.
#if defined IPC_RTE_POSIX
	sleep(1);
	data->bRunning = false;
	ipc_trans_layer_release_recv_wait(data->pid, data->handle);
	ret = pthread_join(data->route_task, NULL);
	if (ret != 0)
		return -ERR_APP_STOP;
#elif defined IPC_RTE_KERNEL
	msleep(1000);
	if (likely(data->route_task)) {
		ipc_trans_layer_release_recv_wait(data->pid, data->handle);
		ret = kthread_stop(data->route_task);
		if (unlikely(ret))
			return -ERR_APP_STOP;
	}
	data->bRunning = false;
#endif

	return RESULT_SUCCESS;
}
#endif
// TimeSyncAdasLogServer_init
TimeSyncAdasLogServer_t *TimeSyncAdasLogServer_init(TimeSyncAdasLogServer_data_t *ins)
{
	int32_t ret = 0;
	com_server_data_t *data = (com_server_data_t *)ins;

	if (!data)
		return NULL;
	if (s_ins && s_ins->com_data.initialized) {
		IPC_LOG_ERR("already initialized.\n");
		return &s_ins->server;
	}

	s_ins = ins;

	data->pid = data->pid == 0 ? PID : data->pid;
	data->fid = data->fid == 0 ? FID : data->fid;
	data->sid = data->sid == 0 ? SID : data->sid;

	// create server handle.
	ret = ipc_trans_layer_stub_create_handle(data->pid, data->fid, data->sid,
				data->pid, &data->handle);
	if (ret < 0)
	{
		IPC_LOG_ERR("create handle fail %d.\n", ret);
		return NULL;
	}

	// init servers
	ret = adas_log_server_init(data, &ins->server.adas_log_server, &ins->adas_log_ext);
	if (ret < 0) {
		(void)ipc_trans_layer_unregister_method(data->pid, data->handle);
		(void)ipc_trans_layer_destroy_handle(data->pid, data->handle);
		return NULL;
	}

#ifdef IPC_RTE_BAREMETAL
	ins->server.receive_message = receive_message;
	ins->server.dispatch_message = dispatch_message;
#else
	ins->server.start = start;
	ins->server.stop = stop;
#endif
	IPC_MUTEX_INIT(&data->send_mtx);
	data->initialized = true;
	return &ins->server;
}

// TimeSyncAdasLogServer_destroy
int32_t TimeSyncAdasLogServer_destroy(void)
{
	int32_t ret = 0;
	com_server_data_t *data = (com_server_data_t *)s_ins;

	// if is NULL, just return SUCCESS.
	if (!data)
		return RESULT_SUCCESS;

	ret = ipc_trans_layer_unregister_method(data->pid, data->handle);
	if (ret < 0)
		return ret;
	ret = ipc_trans_layer_destroy_handle(data->pid, data->handle);
	if (ret < 0)
		return ret;

	adas_log_server_destroy();

	IPC_MUTEX_DESTROY(&data->send_mtx);
	ipc_memset(s_ins, 0, sizeof(TimeSyncAdasLogServer_data_t));
	s_ins = NULL;
	return ret;
}
