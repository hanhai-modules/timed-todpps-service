/* SPDX-License-Identifier: GPL-2.0 OR Apache 2.0
 *
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * This program is also distributed under the terms of the Apache 2.0
 * License.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This file is auto generated for message box v1.2.0.
 * All manual modifications will be LOST by next generation.
 * It is recommended NOT modify it.
 * Generator Version: francaidl 20bed9c msgbx_ipc e82b7ce
 */

#ifndef TIMESYNCADASLOGSERVER_H
#define TIMESYNCADASLOGSERVER_H

#include "adas_log_server.h"

#ifdef __cplusplus
extern "C" {
#endif

struct _TimeSyncAdasLogServer_t {
	adas_log_server_t adas_log_server;

#ifdef IPC_RTE_BAREMETAL
	/**
	 * Receive a message from the server.
	 *
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*receive_message)(void);

	/**
	 * Dispatch a message to the server.
	 *
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*dispatch_message)(void);
#else
	/**
	 * Start the message router.
	 *
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*start)(void);

	/**
	 * Stop the message router.
	 *
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*stop)(void);
#endif
};
#define TimeSyncAdasLogServer_t struct _TimeSyncAdasLogServer_t


/**
 *  The internal data used by the server.
 *  Users should define an instance and pass it to the server initialization function.
 */
struct _TimeSyncAdasLogServer_data_t {
	com_server_data_t com_data;
	TimeSyncAdasLogServer_t server;
	adas_log_server_ext_t adas_log_ext;
};
#define TimeSyncAdasLogServer_data_t struct _TimeSyncAdasLogServer_data_t

/**
 * Initializes the server.
 *
 * @param ins The instance to be initialized.
 * @return A pointer to the initialized server, NULL if fail.
 */
TimeSyncAdasLogServer_t *TimeSyncAdasLogServer_init(TimeSyncAdasLogServer_data_t *ins);
/**
 * Destroys the server.
 *
 * @return 0 if success, negetive if fail.
 */
int32_t TimeSyncAdasLogServer_destroy(void);

#ifdef __cplusplus
}
#endif

#endif // TIMESYNCADASLOGSERVER_H
