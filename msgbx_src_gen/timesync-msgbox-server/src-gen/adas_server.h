/* SPDX-License-Identifier: GPL-2.0 OR Apache 2.0
 *
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * This program is also distributed under the terms of the Apache 2.0
 * License.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This file is auto generated for message box v1.2.0.
 * All manual modifications will be LOST by next generation.
 * It is recommended NOT modify it.
 * Generator Version: francaidl 20bed9c msgbx_ipc e82b7ce
 */

#ifndef ADAS_SERVER_H
#define ADAS_SERVER_H

#define IPC_RTE_POSIX
#ifdef IPC_RTE_KERNEL
#include <bst/ipc_app_svr_utils.h>
#else
#include "ipc_app_svr_utils.h"
#endif
#include "adas_datatype.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Stub function for method adas_timesync.
 *
 * @param sec The input argument of method adas_timesync.
 * @param nsec The input argument of method adas_timesync.
 * @param hicnt The input argument of method adas_timesync.
 * @param lwcnt The input argument of method adas_timesync.
 * @param info The extended information, containing uuid and timestamp.
 */
typedef void (*adas_adas_timesync_t)(
				const uint32_t sec,
				const uint32_t nsec,
				const uint32_t hicnt,
				const uint32_t lwcnt,
				const ext_info_t *info
				);
/**
 * Stub function for method adas_timesync_mode_set.
 *
 * @param tsync_config The input argument of method adas_timesync_mode_set.
 * @param context The message context, to be passed to the reply function.
 * @param info The extended information, containing uuid and timestamp.
 */
typedef void (*adas_adas_timesync_mode_set_t)(
				const adas_TimeSyncConfig_t *tsync_config,
				const uint64_t context,
				const ext_info_t *info
				);

// Interface server
struct _adas_server_t {
	/**
	 * Get the version of the interface.
	 *
	 * @return The version struct, containing major and minor.
	 */
	ipc_inf_version_t (*version)(void);

	/**
	 * Register stub function for method adas_timesync.
	 *
	 * @param func The stub function to be registered.
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*register_adas_timesync)(adas_adas_timesync_t func);

	/**
	 * Register stub function for method adas_timesync_mode_set.
	 *
	 * @param func The stub function to be registered.
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*register_adas_timesync_mode_set)(adas_adas_timesync_mode_set_t func);

	/**
	 * Send the reply message of method adas_timesync_mode_set.
	 *
	 * @param result_status The output argument of method adas_timesync_mode_set.
	 * @param err The error code returned to the client.
	 * @param context The message context, got from the method stub function.
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*reply_adas_timesync_mode_set)(
					const uint32_t result_status,
					const adas_ErrorEnum_t err,
					const uint64_t context);


	/**
	 * Dispatch server messages.
	 *
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*dispatch_request)(serdes_t *des, bool *reply);

	/**
	 * Export registry map.
	 *
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*export_registry_map)(uint8_t *reg_map, const size_t size, size_t *require_size);

	/**
	 * Load registry map.
	 *
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*load_registry_map)(uint8_t *reg_map, const size_t size);
};
#define adas_server_t struct _adas_server_t

/**
 *  The extend data used by the server.
 */
struct _adas_server_ext_t {
	adas_adas_timesync_t adas_timesync_ptr;
	adas_adas_timesync_mode_set_t adas_timesync_mode_set_ptr;

};
#define adas_server_ext_t struct _adas_server_ext_t

/**
 * Initializes the server.
 *
 * @param data The data for com_server_data_t
 * @param server The data for test_server_t
 * @param ext The data for test_server_ext_t
 * @return 0 if success, negative if fail.
 */
int32_t adas_server_init(com_server_data_t *data,
			adas_server_t *server,
			adas_server_ext_t *ext);

/**
 * Destroys the test client.
 */
void adas_server_destroy(void);

#ifdef __cplusplus
}
#endif

#endif // ADAS_SERVER_H
