// SPDX-License-Identifier: GPL-2.0 OR Apache 2.0
/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * This program is also distributed under the terms of the Apache 2.0
 * License.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This file is auto generated for message box v1.2.0.
 * All manual modifications will be LOST by next generation.
 * It is recommended NOT modify it.
 * Generator Version: francaidl 20bed9c msgbx_ipc e82b7ce
 */

#include "adas_server.h"
#ifdef IPC_RTE_KERNEL
#include <bst/ipc_trans_common.h>
#include <bst/ipc_trans_layer.h>
#else
#include "ipc_trans_common.h"
#include "ipc_trans_layer.h"
#endif

// macro definitions
#define MAJOR 2U
#define MINOR 0U

#define MAX_METHOD_NUM 5U
#define MAX_BROADCAST_NUM 20U

#define CMD_METHOD_ADAS_TIMESYNC 1U
#define CMD_METHOD_ADAS_TIMESYNC_MODE_SET 2U


// local variables
static com_server_data_t *s_data = NULL;
static adas_server_ext_t *s_ext = NULL;

// interface implementation
// get interface version
static ipc_inf_version_t get_ipc_inf_version(void)
{
	ipc_inf_version_t ret = { .major = MAJOR, .minor = MINOR };

	return ret;
}

// method

static int32_t register_adas_timesync(adas_adas_timesync_t func)
{
	if (!s_ext)
		return -ERR_APP_PARAM;
	s_ext->adas_timesync_ptr = func;
	return RESULT_SUCCESS;
}

static int32_t call_adas_timesync(serdes_t *des)
{
	int32_t ret = 0;
	des_buf_t *buf = NULL;
	uint32_t len = 0;
	com_server_data_t *data = s_data;
	uint32_t sec = 0;
	uint32_t nsec = 0;
	uint32_t hicnt = 0;
	uint32_t lwcnt = 0;

	if (!des || !data || !s_ext || !s_ext->adas_timesync_ptr)
		return -ERR_APP_PARAM;

	buf = &data->des_buf;
	clear_des_buf(buf);
	len = ipc_des_get_all(des, (uint8_t *)buf->data_buf);
	if (len <= 0)
		return -ERR_APP_SERDES;
	buf->unavail_data_size = IPC_MAX_DATA_SIZE - len;

	if (ret >= 0)
		ret = deserialize_32(buf, (uint32_t *)&sec);
	if (ret >= 0)
		ret = deserialize_32(buf, (uint32_t *)&nsec);
	if (ret >= 0)
		ret = deserialize_32(buf, (uint32_t *)&hicnt);
	if (ret >= 0)
		ret = deserialize_32(buf, (uint32_t *)&lwcnt);

	if (ret < 0)
		return -ERR_APP_SERDES;

	data->info.uuid = ipc_msg_get_uuid(des->header);
	data->info.timestamp = des->recv_end_time;
	(*s_ext->adas_timesync_ptr)(sec, nsec, hicnt, lwcnt, &data->info);

	return RESULT_SUCCESS;
}

static int32_t register_adas_timesync_mode_set(adas_adas_timesync_mode_set_t func)
{
	if (!s_ext)
		return -ERR_APP_PARAM;
	s_ext->adas_timesync_mode_set_ptr = func;
	return RESULT_SUCCESS;
}

static int32_t call_adas_timesync_mode_set(serdes_t *des)
{
	int32_t ret = 0;
	des_buf_t *buf = NULL;
	uint32_t len = 0;
	com_server_data_t *data = s_data;
	uintptr_t pcon = 0;
	adas_TimeSyncConfig_t *tsync_config = NULL;

	if (!des || !data || !s_ext || !s_ext->adas_timesync_mode_set_ptr)
		return -ERR_APP_PARAM;

	buf = &data->des_buf;
	clear_des_buf(buf);
	len = ipc_des_get_all(des, (uint8_t *)buf->data_buf);
	if (len <= 0)
		return -ERR_APP_SERDES;
	buf->unavail_data_size = IPC_MAX_DATA_SIZE - len;

	if (ret >= 0)
		ret = deserialize_adas_TimeSyncConfig(buf, &tsync_config);

	if (ret < 0)
		return -ERR_APP_SERDES;

	data->info.uuid = ipc_msg_get_uuid(des->header);
	data->info.timestamp = des->recv_end_time;
	pcon = (uintptr_t)&des->header;
	(*s_ext->adas_timesync_mode_set_ptr)(tsync_config, *(uint64_t *)pcon, &data->info);

	return RESULT_SUCCESS;
}

static int32_t reply_adas_timesync_mode_set(
				const uint32_t result_status,
				const adas_ErrorEnum_t err,
				const uint64_t context)
{
	int32_t ret = 0;
#ifdef IPC_SHARED_SERIALIZER
	serdes_t *ser = NULL;
#else
	serdes_t serdes = { 0 };
	serdes_t *ser = &serdes;
#endif
	com_server_data_t *data = s_data;
	uintptr_t pcon = (uintptr_t)&context;

	if (!data)
		return -ERR_APP_PARAM;
#ifdef IPC_SHARED_SERIALIZER
	ser = &data->serializer;
#endif
	ret = ipc_ser_init(ser);
	if (ret >= 0)
		ret = serialize_adas_ErrorEnum(ser, &err);
	if (ret >= 0)
		ret = ipc_ser_put_32(ser, (uint32_t *)&result_status);

	if (ret < 0) {
		int32_t _err = -1;
		(void)ipc_ser_init(ser);
		ret = ipc_ser_put_32(ser, (uint32_t *)&_err);
		IPC_LOG_ERR("serialization failed.\n");
	}

	if (ret >= 0) {
		ser->header = *(rw_msg_header_t *)pcon;
		ser->header.cid = ser->header.pid;
		ser->header.pid = data->pid;
		ser->header.typ = MSGBX_MSG_TYPE_REPLY;
		ret = ipc_ser_finish(ser);
	}

	if (ret >= 0)
		ret = send_reply(data, ser);

	if (ret < 0) {
		IPC_LOG_ERR("send reply fail %d.\n", ret);
		return ret;
	}

	return RESULT_SUCCESS;
}

// broadcast


static int32_t export_registry_map(uint8_t *reg_map, const size_t size, size_t *require_size)
{
	int32_t size_cnt = 0;

	if (!reg_map)
		return -1;

	// get all broadcast subscribed registry map size


	*require_size = size_cnt * IPC_REG_MAP_BLOCK_SIZE;
	if (size < *require_size) {
		return -ERR_APP_EXPORT_REG_MAP_SIZE_ERR;
	}

	// export all broadcast subscribed registry map into reg_map


	return 0;
}

static int32_t load_registry_map(uint8_t *reg_map, const size_t size)
{
	uint8_t cmd = 0;
	uint32_t idx = 0;
	uint8_t pid, fid, sid;
	int32_t ret_size = 0;
	uint32_t pos = 0;

	if (!reg_map || size == 0 || (size % IPC_REG_MAP_BLOCK_SIZE != 0))
		return -1;

	for(idx = 0; idx < size / IPC_REG_MAP_BLOCK_SIZE; ++idx) {
		des_registry(reg_map, &pos, &pid, &fid, &sid, &cmd);
		IPC_LOG_INFO("%s pid %x fid %x sid %x cmd %x", __func__, pid, fid, sid, cmd);

		// add all registry info in broadcast entries
		switch (cmd) {
	
		default:
			break;
		}
	}

	return (ret_size * IPC_REG_MAP_BLOCK_SIZE);
}

// dispatch_request
static int32_t dispatch_request(serdes_t *des, bool *reply)
{
	int32_t ret = 0;

	if (!des)
		return -ERR_APP_PARAM;

	switch (des->header.cmd) {
	case CMD_METHOD_ADAS_TIMESYNC:
		ret = call_adas_timesync(des);
		*reply = false;
		return ret;
	case CMD_METHOD_ADAS_TIMESYNC_MODE_SET:
		ret = call_adas_timesync_mode_set(des);
		if (ret >= 0)
			*reply = false;
		else
			*reply = true;
		return ret;

	default:
		break;
	}
	ret = -ERR_APP_UNKNOWN_CMD;
	*reply = false;
	return ret;
}

// initialize server
int32_t adas_server_init(com_server_data_t *data, adas_server_t *server,
			adas_server_ext_t *ext)
{
	int32_t ret = 0;

	if (!data || !server || !ext)
		return -1;

	s_data = data;
	s_ext = ext;

	// register CMDs.
	ret = ipc_trans_layer_register_method(data->pid, data->handle, CMD_METHOD_ADAS_TIMESYNC);
	if (ret < 0)
		return -1;
	ret = ipc_trans_layer_register_method(data->pid, data->handle, CMD_METHOD_ADAS_TIMESYNC_MODE_SET);
	if (ret < 0)
		return -1;

	// set server
	server->version = get_ipc_inf_version;
	ext->adas_timesync_ptr = NULL;
	server->register_adas_timesync = register_adas_timesync;
	ext->adas_timesync_mode_set_ptr = NULL;
	server->register_adas_timesync_mode_set = register_adas_timesync_mode_set;
	server->reply_adas_timesync_mode_set = reply_adas_timesync_mode_set;


	server->export_registry_map = export_registry_map;
	server->load_registry_map = load_registry_map;
	server->dispatch_request = dispatch_request;

	return 0;
}

// destroy client
void adas_server_destroy(void)
{
	s_data = NULL;
	s_ext = NULL;
}
