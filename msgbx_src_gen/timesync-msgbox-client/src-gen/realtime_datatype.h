/* SPDX-License-Identifier: GPL-2.0 OR Apache 2.0
 *
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * This program is also distributed under the terms of the Apache 2.0
 * License.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This file is auto generated for message box v1.2.0.
 * All manual modifications will be LOST by next generation.
 * It is recommended NOT modify it.
 * Generator Version: francaidl 20bed9c msgbx_ipc e82b7ce
 */

#ifndef REALTIME_DATATYPE_H
#define REALTIME_DATATYPE_H

#ifdef IPC_RTE_KERNEL
#include <bst/ipc_app_common.h>
#else
#include "ipc_app_common.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

// user defined types
enum _realtime_ErrorEnum_t {
	REALTIME_NO_ERROR = 0,
	REALTIME_SERVER_FAIL = -1,
	REALTIME_ENABLE_FAIL = -2,
	REALTIME_ERROR_3 = -3
};
#define realtime_ErrorEnum_t enum _realtime_ErrorEnum_t

struct _realtime_TimeSyncConfig_t {
	uint32_t todpps_mode;
	uint32_t cansync_mode;
	uint32_t gptp_mode;
	uint32_t ptp_mode;
};
#define realtime_TimeSyncConfig_t struct _realtime_TimeSyncConfig_t

// constants


// type serialize / deserialize functions
/**
 * Serialize realtime_ErrorEnum_t.
 *
 * @param ser Pointer to serdes_t, which stores the serialized data.
 * @param in The input data to be serialized.
 * @return 0 if success, negative if fail.
 */
static inline int32_t serialize_realtime_ErrorEnum(
							serdes_t *ser,
							const realtime_ErrorEnum_t *in)
{
	int32_t ret = 0;
	uint32_t val = (uint32_t)(*in);

	ret = ipc_ser_put_32(ser, (uint32_t *)&val);
	return ret >= 0 ? 0 : -1;
}

/**
 * Deserialize realtime_ErrorEnum_t.
 *
 * @param des Pointer to serdes_t, which stores the serialized data.
 * @param out The output of deserialized data.
 * @param buf The buffer used to make the deserialized objects.
 * @return 0 if success, negative if fail.
 */
static inline int32_t deserialize_realtime_ErrorEnum(
							des_buf_t *buf,
							realtime_ErrorEnum_t *out)
{
	uint32_t *ptr = NULL;

	if (!out || !buf)
		return -1;

	ptr = (uint32_t *)alloc_data(buf, 4, 4);
	if (!ptr)
		return -1;
	*out = (realtime_ErrorEnum_t)(*ptr);
	return 0;
}

/**
 * Serialize realtime_TimeSyncConfig_t.
 *
 * @param ser Pointer to serdes_t, which stores the serialized data.
 * @param in The input data to be serialized.
 * @return 0 if success, negative if fail.
 */
static inline int32_t serialize_realtime_TimeSyncConfig(
							serdes_t *ser,
							const realtime_TimeSyncConfig_t *in)
{
	int32_t ret = 0;

	ret = ipc_ser_put_align(ser, (const uint8_t *)in,
				sizeof(realtime_TimeSyncConfig_t), 4);
	return ret >= 0 ? 0 : -1;
}

/**
 * Deserialize realtime_TimeSyncConfig_t.
 *
 * @param des Pointer to serdes_t, which stores the serialized data.
 * @param out The output of deserialized data.
 * @param buf The buffer used to make the deserialized objects.
 * @return 0 if success, negative if fail.
 */
static inline int32_t deserialize_realtime_TimeSyncConfig(
							des_buf_t *buf,
							realtime_TimeSyncConfig_t **out)
{
	if (!out || !buf)
		return -1;

	*out = (realtime_TimeSyncConfig_t *)alloc_data(buf, sizeof(realtime_TimeSyncConfig_t), 4);
	if (!*out)
		return -1;

	return 0;
}



#ifdef __cplusplus
}
#endif

#endif
