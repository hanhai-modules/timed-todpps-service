/* SPDX-License-Identifier: GPL-2.0 OR Apache 2.0
 *
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * This program is also distributed under the terms of the Apache 2.0
 * License.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This file is auto generated for message box v1.2.0.
 * All manual modifications will be LOST by next generation.
 * It is recommended NOT modify it.
 * Generator Version: francaidl 20bed9c msgbx_ipc e82b7ce
 */

#ifndef REALTIME_CLIENT_H
#define REALTIME_CLIENT_H

#define IPC_RTE_POSIX
#ifdef IPC_RTE_KERNEL
#include <bst/ipc_app_client_utils.h>
#else
#include "ipc_app_client_utils.h"
#endif
#include "realtime_datatype.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Callback function for realtime_timesync_mode_set_async method.
 *
 * @param result_status The output argument returned by realtime_timesync_mode_set_async.
 * @param err The error code returned by the method.
 * @param ext The user-defined data passed to the method.
 * @param info The extended information, containing uuid and timestamp.
 * @note all the data are stored in ext_buf passed to async call.
 * If ext_buf is NULL, internal buffer will be used.
 * Please note that, the internal buffer is shared by all callbacks.
 * The data MAY CHANGED after leaving the callback function.
 */
typedef void (*realtime_realtime_timesync_mode_set_callback_t)(
				const uint32_t result_status,
				const realtime_ErrorEnum_t err,
				void *ext,
				const ext_info_t *info
				);


// Interface client
struct _realtime_client_t {
	/**
	 * Get the version of the interface.
	 *
	 * @return The version struct, containing major and minor.
	 */
	ipc_inf_version_t (*version)(void);

	/**
	 * Register server availability changed callback.
	 *
	 * @param cb The callback function to be registered.
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*register_avail_changed)(avail_changed_callback_t cb,
					void *ext);

	/**
	 * Fire and forget call to the no_reply_method.
	 * This is one way method call. The server will NOT return.
	 *
	 * @param sec The input argument of method realtime_timesync.
	 * @param nsec The input argument of method realtime_timesync.
	 * @param hicnt The input argument of method realtime_timesync.
	 * @param lwcnt The input argument of method realtime_timesync.
	 * @return 0 if success, negative if fail.
	 * @note This is unreliable transmission, be used ONLY if message losing is accepted.
	 */
	int32_t (*realtime_timesync_fire_and_forget)(
					const uint32_t sec,
					const uint32_t nsec,
					const uint32_t hicnt,
					const uint32_t lwcnt
					);

	#ifndef IPC_RTE_BAREMETAL
	/**
	 * Synchronously call the hello method.
	 *
	 * @param tsync_config The input argument of method realtime_timesync_mode_set.
	 * @param result_status The output argument of method realtime_timesync_mode_set.
	 * @param err The error code returned by the method.
	 * @param timeout_ms The timeout for the method call in milliseconds, less or equal to 0 means wait forever.
	 * @param ext_buf The buffer to store the user-defined data.
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*realtime_timesync_mode_set_sync)(
					const realtime_TimeSyncConfig_t *tsync_config,
					uint32_t *result_status,
					realtime_ErrorEnum_t *err,
					int64_t timeout_ms,
					des_buf_t *ext_buf
					);
	#endif

	/**
	 * Asynchronously call the realtime_timesync_mode_set method.
	 *
	 * @param tsync_config The input argument of method realtime_timesync_mode_set.
	 * @param cb The callback function to be called when the method returns.
	 * @param ext The user-defined data passed to the method.
	 * @param ext_buf The buffer to store the user-defined data.
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*realtime_timesync_mode_set_async)(
					const realtime_TimeSyncConfig_t *tsync_config,
					realtime_realtime_timesync_mode_set_callback_t cb,
					void *ext,
					des_buf_t *ext_buf
					);


	/**
	 * Dispatch broadcast messages.
	 *
	 * @param des The received message package.
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*dispatch_broadcast)(serdes_t *des);

	/**
	 * Dispatch reply messages.
	 *
	 * @param des The received message package.
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*dispatch_reply)(serdes_t *des);
};
#define realtime_client_t struct _realtime_client_t

/**
 *  The extend data used by the client.
 */
struct _realtime_client_ext_t {
	uint8_t cid;
	uint8_t res[7];

};
#define realtime_client_ext_t struct _realtime_client_ext_t

/**
 * Initializes the client.
 *
 * @param data The data for com_client_data_t
 * @param client The data for realtime_client_t
 * @param ext The data for realtime_client_ext_t
 * @return 0 if success, negative if fail.
 */
int32_t realtime_client_init(com_client_data_t *data,
			realtime_client_t *client,
			realtime_client_ext_t *ext);

/**
 * Destroys the client.
 */
void realtime_client_destroy(void);

#ifdef __cplusplus
}
#endif

#endif // REALTIME_CLIENT_H
