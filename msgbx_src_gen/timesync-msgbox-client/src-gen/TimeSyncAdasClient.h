/* SPDX-License-Identifier: GPL-2.0 OR Apache 2.0
 *
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * This program is also distributed under the terms of the Apache 2.0
 * License.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This file is auto generated for message box v1.2.0.
 * All manual modifications will be LOST by next generation.
 * It is recommended NOT modify it.
 * Generator Version: francaidl 20bed9c msgbx_ipc e82b7ce
 */

#ifndef TIMESYNCADASCLIENT_H
#define TIMESYNCADASCLIENT_H

#include "realtime_client.h"
#include "switch_client.h"
#include "db_client.h"
#include "ivi_client.h"

#ifdef __cplusplus
extern "C" {
#endif

struct _TimeSyncAdasClient_t {
	realtime_client_t realtime_client;
	switch_client_t switch_client;
	db_client_t db_client;
	ivi_client_t ivi_client;

#ifdef IPC_RTE_BAREMETAL
	/**
	 * Receive a message from the server.
	 *
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*receive_message)(void);

	/**
	 * Dispatch a message to the server.
	 *
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*dispatch_message)(void);
#else
	/**
	 * Start the message router.
	 *
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*start)(void);

	/**
	 * Stop the message router.
	 *
	 * @return 0 if success, negative if fail.
	 */
	int32_t (*stop)(void);
#endif
};
#define TimeSyncAdasClient_t struct _TimeSyncAdasClient_t

/**
 *  The internal data used by the client.
 *  Users should define an instance and pass it to the initialization function.
 */
struct _TimeSyncAdasClient_data_t {
	com_client_data_t com_data;
	TimeSyncAdasClient_t client;
	realtime_client_ext_t realtime_ext;
	switch_client_ext_t switch_ext;
	db_client_ext_t db_ext;
	ivi_client_ext_t ivi_ext;
};
#define TimeSyncAdasClient_data_t struct _TimeSyncAdasClient_data_t

/**
 * Initializes the client.
 *
 * @param data The data to be used by the client.
 * @return A pointer to the initialized client, NULL if fail.
 */
TimeSyncAdasClient_t *TimeSyncAdasClient_init(TimeSyncAdasClient_data_t *ins);

/**
 * Destroys the client.
 *
 * @return 0 if success, negetive if fail.
 */
int32_t TimeSyncAdasClient_destroy(void);

#ifdef __cplusplus
}
#endif

#endif // TIMESYNCADASCLIENT_H
