// SPDX-License-Identifier: GPL-2.0 OR Apache 2.0
/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * This program is also distributed under the terms of the Apache 2.0
 * License.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This file is auto generated for message box v1.2.0.
 * All manual modifications will be LOST by next generation.
 * It is recommended NOT modify it.
 * Generator Version: francaidl 20bed9c msgbx_ipc e82b7ce
 */

#include "switch_client.h"

// macro definitions
#define CID SWITCH_1
#define MAJOR 2U
#define MINOR 0U


#define CMD_METHOD_SWITCH_TIMESYNC 9U
#define CMD_METHOD_SWITCH_TIMESYNC_MODE_SET 8U


// local variables
static com_client_data_t *s_data = NULL;
static switch_client_ext_t *s_ext = NULL;

#ifndef IPC_RTE_BAREMETAL

struct _switch_timesync_mode_set_out_t {
	uint32_t *result_status;
	switch_ErrorEnum_t *err;
};
#define switch_timesync_mode_set_out_t struct _switch_timesync_mode_set_out_t

#endif
// interface implementation
// get interface version
static ipc_inf_version_t get_ipc_inf_version(void)
{
	ipc_inf_version_t ret = { .major = MAJOR, .minor = MINOR };

	return ret;
}

// method

static inline int32_t serialize_switch_timesync(
				serdes_t *ser,
				const uint32_t sec,
				const uint32_t nsec,
				const uint32_t hicnt,
				const uint32_t lwcnt
				)
{
	int32_t ret = 0;

	if (ret >= 0)
		ret = ipc_ser_put_32(ser, (uint32_t *)&sec);
	if (ret >= 0)
		ret = ipc_ser_put_32(ser, (uint32_t *)&nsec);
	if (ret >= 0)
		ret = ipc_ser_put_32(ser, (uint32_t *)&hicnt);
	if (ret >= 0)
		ret = ipc_ser_put_32(ser, (uint32_t *)&lwcnt);

	if (ret < 0)
		return -ERR_APP_SERDES;
	else
		return RESULT_SUCCESS;
}

static int32_t call_switch_timesync_fire_and_forget(const uint32_t sec,
				const uint32_t nsec,
				const uint32_t hicnt,
				const uint32_t lwcnt)
{
	int32_t ret = 0;
#ifdef IPC_SHARED_SERIALIZER
	serdes_t *ser = NULL;
#else
	serdes_t serdes = { 0 };
	serdes_t *ser = &serdes;
#endif
	com_client_data_t *data = s_data;

	if (!data || !s_ext)
		return -ERR_APP_PARAM;
#ifdef IPC_SHARED_SERIALIZER
	ser = &data->serializer;
#endif
	(void)ipc_ser_init(ser);

	ret = serialize_switch_timesync(ser, sec, nsec, hicnt, lwcnt);
	if (ret != 0) {
		IPC_LOG_ERR("serialize fail.\n");
		return -ERR_APP_SERDES;
	}

	// send request
	ret = send_fire_and_forget_request(data, ser, s_ext->cid, CMD_METHOD_SWITCH_TIMESYNC);
	if (ret < 0) {
		IPC_LOG_ERR("send method fail %d.\n", ret);
		return ret;
	}

	return RESULT_SUCCESS;
}

static inline int32_t serialize_switch_timesync_mode_set(
				serdes_t *ser,
				const switch_TimeSyncConfig_t *tsync_config
				)
{
	int32_t ret = 0;

	if (ret >= 0)
		ret = serialize_switch_TimeSyncConfig(ser, tsync_config);

	if (ret < 0)
		return -ERR_APP_SERDES;
	else
		return RESULT_SUCCESS;
}
#ifndef IPC_RTE_BAREMETAL

static void switch_timesync_mode_set_sync_callback(
				const uint32_t result_status,
				const switch_ErrorEnum_t err,
				void *ext,
				const ext_info_t *info
				)
{
	switch_timesync_mode_set_out_t *out = (switch_timesync_mode_set_out_t *)ext;

	if (!out)
		return;
	*out->result_status = result_status;
	*out->err = err;
}

static int32_t call_switch_timesync_mode_set_sync(const switch_TimeSyncConfig_t *tsync_config,
				uint32_t *result_status,
				switch_ErrorEnum_t *err,
				int64_t timeout_ms,
				des_buf_t *ext_buf)
{
	int32_t ret = 0;
	serdes_t serdes = { 0 };
	serdes_t *ser = NULL;
	switch_timesync_mode_set_out_t out = {.result_status = result_status,
				.err = err};
	callback_registration_t *reg = NULL;
	com_client_data_t *data = s_data;

	if (!data || !s_ext)
		return -ERR_APP_PARAM;
	ser = &serdes;
	(void)ipc_ser_init(ser);

	ret = serialize_switch_timesync_mode_set(ser, tsync_config);
	if (ret != 0) {
		IPC_LOG_ERR("serialize fail.\n");
		return -ERR_APP_SERDES;
	}

	// send request
	ret = send_request(data, ser, s_ext->cid, CMD_METHOD_SWITCH_TIMESYNC_MODE_SET,
				switch_timesync_mode_set_sync_callback, &out, ext_buf);
	if (ret < 0 || ret >= IPC_TOKEN_NUM) {
		IPC_LOG_ERR("send method fail %d.\n", ret);
		return ret;
	}

	//wait for reply
	reg = &data->method_registry[ret];
	reg->disable_gc = true;
	if (timeout_ms <= 0)
		ret = wait_on_registry(reg);
	else
		ret = timedwait_on_registry(reg, timeout_ms);
	if (ret < 0) {
		clear_registry(reg);
		IPC_LOG_ERR("wait timeout\n");
	}

	return ret;
}
#endif

static int32_t call_switch_timesync_mode_set_async(const switch_TimeSyncConfig_t *tsync_config,
				switch_switch_timesync_mode_set_callback_t cb,
				void *ext,
				des_buf_t *ext_buf)
{
	int32_t ret = 0;
#ifdef IPC_SHARED_SERIALIZER
	serdes_t *ser = NULL;
#else
	serdes_t serdes = { 0 };
	serdes_t *ser = &serdes;
#endif
	com_client_data_t *data = s_data;

	if (!data || !s_ext)
		return -ERR_APP_PARAM;
#ifdef IPC_SHARED_SERIALIZER
	ser = &data->serializer;
#endif
	(void)ipc_ser_init(ser);

	ret = serialize_switch_timesync_mode_set(ser, tsync_config);
	if (ret != 0) {
		IPC_LOG_ERR("serialize fail.\n");
		return -ERR_APP_SERDES;
	}

	// send request
	ret = send_request(data, ser, s_ext->cid, CMD_METHOD_SWITCH_TIMESYNC_MODE_SET,
				cb, ext, ext_buf);
	if (ret < 0 || ret >= IPC_TOKEN_NUM) {
		IPC_LOG_ERR("send method fail %d.\n", ret);
		return ret;
	}

	return RESULT_SUCCESS;
}

static inline int32_t call_switch_timesync_mode_set_callback(serdes_t *des)
{
	int32_t ret = 0;
	callback_registration_t *reg = NULL;
	des_buf_t *buf = NULL;
	uint32_t len = 0;
	com_client_data_t *data = s_data;
	switch_switch_timesync_mode_set_callback_t cb = NULL;
	uint32_t result_status = 0;
	switch_ErrorEnum_t err = 0;


	if (!des || !data)
		return -ERR_APP_PARAM;

	reg = &data->method_registry[des->header.tok];
	if (!reg->busy || reg->cmd != CMD_METHOD_SWITCH_TIMESYNC_MODE_SET) {
		IPC_LOG_ERR("callback registry is invalid.\n");
		return -ERR_APP_TOK;
	}
	buf = reg->ext_buf ? reg->ext_buf : &data->des_buf;
	clear_des_buf(buf);
	data->info.uuid = ipc_msg_get_uuid(des->header);
	data->info.timestamp = des->recv_end_time;

	// deserialize arguments
	len = ipc_des_get_all(des, (uint8_t *)buf->data_buf);
	if (len <= 0)
		return -ERR_APP_SERDES;
	buf->unavail_data_size = IPC_MAX_DATA_SIZE - len;

	if (ret >= 0)
		ret = deserialize_switch_ErrorEnum(buf, &err);

	if (ret < 0)
		return -ERR_APP_SERDES;
	if (err == SWITCH_NO_ERROR) {
		if (ret >= 0)
			ret = deserialize_32(buf, (uint32_t *)&result_status);
		if (ret < 0)
			return -ERR_APP_SERDES;
	}

	// call callback function
	cb = (switch_switch_timesync_mode_set_callback_t)(reg->cb);
	if (cb)
		cb(result_status, err, reg->ext, &data->info);
#ifndef IPC_RTE_BAREMETAL
	notify_callback_registry(reg);
#endif
	clear_registry(reg);

	return RESULT_SUCCESS;
}

// broadcast

// dispatch_broadcast
static inline int32_t dispatch_broadcast(serdes_t *des)
{
	int32_t ret = 0;

	if (!des || des->header.pid != s_ext->cid)
		return -ERR_APP_PARAM;

	switch (des->header.cmd) {

	default:
		ret = -ERR_APP_UNKNOWN_CMD;
		IPC_LOG_ERR("unknown broadcast message %d.\n", des->header.cmd);
		break;
	}

	return ret;
}

// dispatch_reply
static inline int32_t dispatch_reply(serdes_t *des)
{
	int32_t ret = 0;

	if (!des || des->header.pid != s_ext->cid)
		return -ERR_APP_PARAM;

	switch (des->header.cmd) {
	case CMD_METHOD_SWITCH_TIMESYNC_MODE_SET:
		ret = call_switch_timesync_mode_set_callback(des);
		break;

	default:
		ret = -ERR_APP_UNKNOWN_CMD;
		IPC_LOG_ERR("unknown reply message %d.\n", des->header.cmd);
		break;
	}

	return ret;
}

// register availablity changed callback function
static int32_t register_avail_changed_cb(avail_changed_callback_t cb, void *ext)
{
	return reg_avail_changed_cb(s_data, cb, ext);
}

// initialize client
int32_t switch_client_init(com_client_data_t *data, switch_client_t *client,
			switch_client_ext_t *ext)
{
	if (!data || !client || !ext)
		return -1;

	s_data = data;
	s_ext = ext;

	// set client
	client->version = get_ipc_inf_version;
	client->register_avail_changed = register_avail_changed_cb;
	client->switch_timesync_fire_and_forget = call_switch_timesync_fire_and_forget;
#ifndef IPC_RTE_BAREMETAL
	client->switch_timesync_mode_set_sync = call_switch_timesync_mode_set_sync;
#endif
	client->switch_timesync_mode_set_async = call_switch_timesync_mode_set_async;


	client->dispatch_broadcast = dispatch_broadcast;
	client->dispatch_reply = dispatch_reply;

	// set ext
	if (ext->cid == 0)
		ext->cid = CID;

	return 0;
}
// destroy client
void switch_client_destroy(void)
{

	s_data = NULL;
	s_ext = NULL;
}
