This program is released under the Apache2.0 License.

# tod-pps-sync-service

该工程实现todpps同步插件；

## 目录说明

`plugin`目录存放todpps同步插件源代码；
`msgbx_src_gen`目录存放msgbox的生成代码；
`msgbx_wrap`目录存放msgbox的封装层代码；
`examples`目录存放同步日志工具、同步模式设置工具的源代码；

## 编译说明

在工程目录下依次执行下述命令，可完成工程编译：

```shell
mkdir build && cd build
cmake ..
make
```
