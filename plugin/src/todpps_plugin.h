/*
 *Copyright (c) 2024 Black Sesame Technologies
 *
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 */

#ifndef HANHAI_TIMESYNC_TODPPS_INTERFACE_H
#define HANHAI_TIMESYNC_TODPPS_INTERFACE_H

#include <string>
#include <memory>
#include <stdint.h>
#include <time.h>
#include <thread>
#include <linux/ptp_clock.h>
#include <hanhai/timesync/base_timesync_interface.h>

namespace hanhai {
namespace timesync {
namespace protocol {

class TodppsInterface : public BaseTimesyncInterface
{
public:
    /// @brief 默认构造函数
    TodppsInterface() = default;

    /// @brief 默认析构函数
    ~TodppsInterface() = default;

    /// @brief 初始化时间同步插件
    /// @param config_file 插件配置文件
    /// @param ptp_device ptp设备名称
    /// @param utc_offset UTC标度时间与PTP标度时间的偏差值
    /// @param error_code 错误码
    /// @return 成功返回true，失败返回false
    bool Init(const std::string &config_file, const std::string &ptp_device, const int32_t &utc_offset, uint8_t &error_code) override;

    /// @brief 启动时间同步插件
    /// @param error_code 错误码
    /// @return 成功返回true，失败返回false
    bool Start(uint8_t &error_code) override;

    /// @brief 停止时间同步插件
    /// @param error_code 错误码
    /// @return 成功返回true，失败返回false
    bool Stop(uint8_t &error_code) override;

    /// @brief 关闭时间同步插件
    /// @param error_code 错误码
    /// @return 成功返回true，失败返回false
    bool Close(uint8_t &error_code) override;

    /// @brief 获取同步信息
    /// @param error_code 错误码
    /// @return 同步信息结构体
    void GetSyncInfo(SyncInfo *sync_info) override;

private:
    /// @brief 解析配置文件
    /// @param config_file 插件配置文件
    /// @param error_code 错误码
    /// @return 成功返回true，失败返回false
    bool ParseConfig(const std::string &config_file, uint8_t &error_code);

    /// @brief 初始化ptp设备
    /// @param error_code 错误码
    /// @return 成功返回true，失败返回false
    bool InitPtpDevice(uint8_t &error_code);

    /// @brief 对时间戳的采样值做处理
    /// @param offset 与时钟源之间的时间偏差
    /// @param local_ts 本地的pps时间戳
    /// @param servo_status servo状态值
    /// @return 返回频率偏差值，单位ppb
    double OffsetSample(int64_t offset, uint64_t local_ts, uint8_t &servo_status);

    /// @brief 调整时钟频率
    /// @param clkid 时钟ID
    /// @param freq 需要调整的频率值
    /// @return 成功返回0，失败返回-1
    int ClockAdjSetFreq(clockid_t clkid, double freq);

    /// @brief 获取PHC时钟的最大可调频率
    /// @param clkid 时钟ID
    /// @return 成功返回0，失败返回-1
    int PhcMaxAdj(clockid_t clkid);

    /// @brief 获取PHC时钟的能力属性
    /// @param clkid 时钟ID
    /// @param caps 返回时钟能力属性结构体指针
    /// @return 成功返回0，失败返回-1
    int PhcGetCaps(clockid_t clkid, struct ptp_clock_caps *caps);

    /// @brief 调整时钟使时钟步进
    /// @param clkid 时钟ID
    /// @param step 时间的步进值，单位ns
    /// @return 成功返回0，失败返回-1
    int ClockAdjStep(clockid_t clkid, int64_t step);

    /// @brief 使能输出pps信号
    bool EnablePpsOut(void);

    /// @brief todpps master主函数
    void MainFuncMaster(void);

    /// @brief todpps slave主函数
    void MainFuncSlave(void);

private:
    /// @brief ptp设备名称
    std::string m_ptp_device{"/dev/ptp0"};
    /// @brief UTC时间偏差
    int32_t m_utc_offset{0};
    /// @brief 是否同步的标志
    std::atomic_bool m_is_synchronized{false};
    /// @brief 与时钟源之间的偏差值
    int64_t m_master_offset{0};
    /// @brief 与时钟源之间的频率偏差
    double m_freq_offset{0};
    /// @brief 时钟ID
    clockid_t m_clock_id{-1};
    /// @brief 工作模式，master/slave
    std::string m_mode{};
    /// @brief 使能线程停止
    bool m_stop_flag{false};
    /// @brief PTP设备文件描述符
    int32_t m_ptp_fd{-1};
    /// @brief pps信号接收触发引脚
    int32_t m_pps_trig_in_index{3};
    /// @brief pps信号发送触发引脚
    int32_t m_pps_trig_out_index{3};
    /// @brief 同步线程，master或者slave
    std::shared_ptr<std::thread> p_sync_thread{nullptr};
    /// @brief netlink线程
    std::shared_ptr<std::thread> p_netlink_thread{nullptr};
    /// @brief pps信号发送时刻时间戳秒部分
    uint32_t m_pps_out_sec{0};
    /// @brief pps信号发送时刻时间戳纳秒部分
    uint32_t m_pps_out_nsec{0};
    /// @brief 使能pps输出成功的标志
    bool m_enable_pps_out{false};

};  // class TodppsInterface

}   // namespace protocol
}   // namespace timesync
}   // namespace hanhai

#endif  // HANHAI_TIMESYNC_TODPPS_INTERFACE_H
