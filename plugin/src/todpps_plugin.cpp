/*
 *Copyright (c) 2024 Black Sesame Technologies
 *
 *Licensed under the Apache License, Version 2.0 (the "License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing, software
 *distributed under the License is distributed on an "AS IS" BASIS,
 *WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *See the License for the specific language governing permissions and
 *limitations under the License.
 */

#include <hanhai/infra/nlohmann/json.hpp>
#include <fstream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <functional>
#include "timesync_msgbox_server_wrap.h"
#include "timesync_msgbox_client_wrap.h"
#include "netlink.h"
#include "todpps_plugin.h"

#define CLOCKFD 3
#define FROM_FD_TO_CLOCKID(fd) ((clockid_t)((((unsigned int)~fd) << 3) | CLOCKFD))
#define CLOCKID_TO_FD(clk) ((unsigned int)~((clk) >> 3))

#ifndef NSEC_PER_SEC
#define NSEC_PER_SEC 1000000000ULL
#endif

#define BITS_PER_LONG (sizeof(long) * 8)
#define MAX_PPB_32 32767999 /* 2^31 - 1 / 65.536 */

#define KP 0.7
#define KI 0.3
#define STEP_THRESHOLD 1000000000ULL

extern uint32_t g_latch_gtc_hicnt;
extern uint32_t g_latch_gtc_lwcnt;
extern uint32_t g_pps_in_sec;
extern uint32_t g_pps_in_nsec;
extern int32_t g_nl_flag;

static uint64_t g_tod_ts{0};
static uint64_t g_tod_cnt{0};
static int32_t g_tod_flag{0};

namespace hanhai {
namespace timesync {
namespace protocol {

using Json = nlohmann::json;

static void on_tod_rcvd(const uint32_t sec, const uint32_t nsec, const uint32_t hicnt, const uint32_t lwcnt)
{
    uint64_t tod_ts = (uint64_t)sec * 1000000000ULL + nsec;
    uint64_t tod_cnt = (uint64_t)hicnt << 32 | lwcnt;

    if (tod_ts > g_tod_ts && tod_cnt > g_tod_cnt)
    {
        g_tod_ts = tod_ts;
        g_tod_cnt = tod_cnt;
        g_tod_flag = 1;
    }
}

bool TodppsInterface::Init(const std::string &config_file, const std::string &ptp_device, const int32_t &utc_offset, uint8_t &error_code)
{
    // 检查配置文件是否为空
    if (config_file.empty())
    {
        std::cout << "Error unspecified config file for todpps plugin" << std::endl;
        error_code = 18;
        return false;
    }

    // 检查配置文件格式是否为json格式
    if (config_file.find(".json") == std::string::npos)
    {
        std::cout << "Error unsupported config file format" << std::endl;
        error_code = 2;
        return false;
    }

    std::cout << "Using config file for todpps plugin: " << config_file << std::endl;

    // 解析配置文件
    if (!ParseConfig(config_file, error_code))
        return false;

    if (Netlink_Init())
        return false;

    if (!ptp_device.empty())
        m_ptp_device = ptp_device;
    m_utc_offset = utc_offset;

    if (!InitPtpDevice(error_code))
        return false;

    if (m_mode == "slave")
    {
        if (!timesync_msgbox_server_init(0, 0, 0))
            return false;
        timesync_msgbox_server_reg_timesync_func(&on_tod_rcvd);
    }

    if (m_mode == "master" && !timesync_msgbox_client_init(0, 0, 0))
        return false;

    return true;
}

bool TodppsInterface::ParseConfig(const std::string &config_file, uint8_t &error_code)
{
    // 打开配置文件
    std::ifstream config_stream(config_file);
    if (!config_stream.good())
    {
        std::cout << "failed to open " << config_file << std::endl;
        error_code = 3;
        return false;
    }

    Json config{};
    config_stream >> config;
    config_stream.close();

    // 解析mode，该项必须存在
    if (config["mode"].is_null())
    {
        std::cout << "there is no available work mode(master or slave) in config file" << std::endl;
        return false;
    }
    m_mode = config["mode"].get<std::string>();

    if (m_mode != "master" && m_mode != "slave")
    {
        std::cout << "mode is not right, only master and slave supported" << std::endl;
        return false;
    }

    if (!config["pps_trigger_index"].is_null())
    {
        if (m_mode == "slave")
            m_pps_trig_in_index = config["pps_trigger_index"];
        else
            m_pps_trig_out_index = config["pps_trigger_index"];
    }

    return true;
}

bool TodppsInterface::InitPtpDevice(uint8_t &error_code)
{
    // 打开PTP设备
    m_ptp_fd = open(m_ptp_device.c_str(), O_RDWR);
    if (m_ptp_fd < 0)
    {
        std::cout << "failed to open " << m_ptp_device << std::endl;
        error_code = 20;
        return false;
    }

    // 初始化PTP设备
    struct ptp_extts_request extts_request;
    memset(&extts_request, 0, sizeof(extts_request));
    extts_request.flags = 1;
    extts_request.index = m_pps_trig_in_index;

    if (ioctl(m_ptp_fd, PTP_EXTTS_REQUEST, &extts_request))
    {
        std::cout << "failed to request extern timestamp" << std::endl;
        error_code = 21;
        close(m_ptp_fd);
        return false;
    }

    // 获取时钟ID
    m_clock_id = FROM_FD_TO_CLOCKID(m_ptp_fd);
    return true;
}

bool TodppsInterface::Start(uint8_t &error_code)
{
    if (p_netlink_thread != nullptr)
        return false;

    if (p_sync_thread != nullptr)
        return false;
    
    p_netlink_thread = std::make_shared<std::thread>(&Netlink_Start);
    if (p_netlink_thread == nullptr)
    {
        std::cout << "Error creating netlink thread" << std::endl;
        return false;
    }
    pthread_setname_np(p_netlink_thread->native_handle(), "netlink_todpps");

    if (m_mode == "master")
        p_sync_thread = std::make_shared<std::thread>(&TodppsInterface::MainFuncMaster, this);
    if (m_mode == "slave")
        p_sync_thread = std::make_shared<std::thread>(&TodppsInterface::MainFuncSlave, this);
    if (p_sync_thread == nullptr)
    {
        std::cout << "Error creating sync thread" << std::endl;
        return false;
    }
    pthread_setname_np(p_sync_thread->native_handle(), "todpps");

    return true;
}

bool TodppsInterface::EnablePpsOut()
{
    struct timespec ts;
    if (clock_gettime(m_clock_id, &ts))
    {
        std::cout << "Error getting clock time [/dev/ptp0]" << std::endl;
        m_enable_pps_out = false;
        return false;
    }

    struct ptp_perout_request perout_request;
    memset(&perout_request, 0, sizeof(perout_request));
    perout_request.index = m_pps_trig_out_index;
    perout_request.period.sec = 1;
    perout_request.period.nsec = 0;
    perout_request.flags = 0;
    perout_request.start.sec = ts.tv_sec + 2;
    perout_request.start.nsec = 0;
    m_pps_out_sec = perout_request.start.sec;
    m_pps_out_nsec = 0;
    if (ioctl(m_ptp_fd, PTP_PEROUT_REQUEST2, &perout_request))
    {
        std::cout << "Error ioctl PTP_PEROUT_REQUEST2" << std::endl;
        m_enable_pps_out = false;
        return false;
    }

    m_enable_pps_out = true;
    return true;
}

bool TodppsInterface::Stop(uint8_t &error_code)
{
    m_stop_flag = true;
    Netlink_Stop();
    if (p_netlink_thread != nullptr)
    {
        p_netlink_thread->join();
        p_netlink_thread = nullptr;
    }
    
    if (p_sync_thread != nullptr)
    {
        p_sync_thread->join();
        p_sync_thread = nullptr;
    }

    return true;
}

bool TodppsInterface::Close(uint8_t &error_code)
{
    Stop(error_code);
    Netlink_Destroy();

    if (m_mode == "master")
        timesync_msgbox_client_destroy();
    else
        timesync_msgbox_server_destroy();

    close(m_ptp_fd);
    return true;
}

void TodppsInterface::GetSyncInfo(SyncInfo *sync_info)
{
    sync_info->is_synchronized = m_is_synchronized;
    sync_info->master_offset = m_master_offset;
    sync_info->freq_offset = m_freq_offset;
    sync_info->path_delay = 0;
}

int TodppsInterface::ClockAdjStep(clockid_t clkid, int64_t step)
{
    struct timex tx;
    int sign = 1;
    if (step < 0)
    {
        sign = -1;
        step *= -1;
    }
    memset(&tx, 0, sizeof(tx));
    tx.modes = ADJ_SETOFFSET | ADJ_NANO;
    tx.time.tv_sec = sign * (step / NSEC_PER_SEC);
    tx.time.tv_usec = sign * (step % NSEC_PER_SEC);
    /*
        * The value of a timeval is the sum of its fields, but the
        * field tv_usec must always be non-negative.
        */
    if (tx.time.tv_usec < 0)
    {
        tx.time.tv_sec -= 1;
        tx.time.tv_usec += 1000000000;
    }
    if (clock_adjtime(clkid, &tx) < 0)
    {
        std::cout << "failed to step clock" << std::endl;
        return false;
    }
    return 0;
}

int TodppsInterface::ClockAdjSetFreq(clockid_t clkid, double freq)
{
    struct timex tx;
    memset(&tx, 0, sizeof(tx));

    tx.modes |= ADJ_FREQUENCY;
    tx.freq = (long)(freq * 65.536);
    if (clock_adjtime(clkid, &tx) < 0)
    {
        std::cout << "failed to adjust the clock" << std::endl;
        return false;
    }
    return 0;
}

double TodppsInterface::OffsetSample(int64_t offset, uint64_t local_ts, uint8_t &servo_status)
{
    static double last_freq = 0;
    double ki_term, ppb = last_freq;
    double freq_est_interval, localdiff;
    static uint8_t count{0};
    static int64_t offset_buf[2] = {0};
    static uint64_t local_buf[2] = {0};
    static double drift = 0;
    int max_frequency = PhcMaxAdj(m_clock_id);

    switch (count)
    {
    case 0:
        offset_buf[0] = offset;
        local_buf[0] = local_ts;
        servo_status = 0; // unlocked
        count = 1;
        break;
    case 1:
        offset_buf[1] = offset;
        local_buf[1] = local_ts;

        /* Make sure the first sample is older than the second. */
        if (local_buf[0] >= local_buf[1])
        {
            servo_status = 0; // unlocked
            count = 0;
            break;
        }

        /* Wait long enough before estimating the frequency offset. */
        localdiff = (local_buf[1] - local_buf[0]) / 1e9;
        localdiff += localdiff * 0.001;
        freq_est_interval = 0.016 / KI;
        if (freq_est_interval > 1000.0)
        {
            freq_est_interval = 1000.0;
        }
        if (localdiff < freq_est_interval)
        {
            servo_status = 0; // unlocked
            break;
        }

        /* Adjust drift by the measured frequency offset. */
        drift += (1e9 - drift) * (offset_buf[1] - offset_buf[0]) / (local_buf[1] - local_buf[0]);

        if (drift < -max_frequency)
            drift = -max_frequency;
        else if (drift > max_frequency)
            drift = max_frequency;

        if (llabs(offset) > STEP_THRESHOLD)
            servo_status = 1; // jump
        else
            servo_status = 2; // locked

        ppb = drift;
        count = 2;
        break;
    case 2:
        /*
            * reset the clock servo when offset is greater than the max
            * offset value. Note that the clock jump will be performed in
            * step 1, so it is not necessary to have clock jump
            * immediately. This allows re-calculating drift as in initial
            * clock startup.
            */
        if (llabs(offset) > STEP_THRESHOLD)
        {
            servo_status = 0; // unlocked
            count = 0;
            break;
        }

        ki_term = KI * offset;
        ppb = KP * offset + drift + ki_term;
        if (ppb < -max_frequency)
        {
            ppb = -max_frequency;
        }
        else if (ppb > max_frequency)
        {
            ppb = max_frequency;
        }
        else
        {
            drift += ki_term;
        }
        servo_status = 2;
        break;
    }

    last_freq = ppb;
    return ppb;
}

int TodppsInterface::PhcGetCaps(clockid_t clkid, struct ptp_clock_caps *caps)
{
    int fd = CLOCKID_TO_FD(clkid), err;

    err = ioctl(fd, PTP_CLOCK_GETCAPS, caps);
    if (err)
        std::cout << "PTP_CLOCK_GETCAPS" << std::endl;
    return err;
}

int TodppsInterface::PhcMaxAdj(clockid_t clkid)
{
    int max;
    struct ptp_clock_caps caps;

    if (PhcGetCaps(clkid, &caps))
        return 0;

    max = caps.max_adj;

    if (BITS_PER_LONG == 32 && max > MAX_PPB_32)
        max = MAX_PPB_32;

    return max;
}

void TodppsInterface::MainFuncMaster(void)
{
    EnablePpsOut();
    while (!m_stop_flag)
    {
        if (m_enable_pps_out && g_nl_flag == 1)
        {
            g_nl_flag = 0;
            timesync_msgbox_client_send_tod(SWITCH, m_pps_out_sec, m_pps_out_nsec, g_latch_gtc_hicnt, g_latch_gtc_lwcnt);
            timesync_msgbox_client_send_tod(REALTIME, m_pps_out_sec, m_pps_out_nsec, g_latch_gtc_hicnt, g_latch_gtc_lwcnt);
            timesync_msgbox_client_send_tod(DB, m_pps_out_sec, m_pps_out_nsec, g_latch_gtc_hicnt, g_latch_gtc_lwcnt);
            timesync_msgbox_client_send_tod(IVI, m_pps_out_sec, m_pps_out_nsec, g_latch_gtc_hicnt, g_latch_gtc_lwcnt);
            EnablePpsOut();
        }
        else if (!m_enable_pps_out)
            EnablePpsOut();
        
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

void TodppsInterface::MainFuncSlave(void)
{
    while (!m_stop_flag)
    {
        if (g_tod_flag == 1 && g_nl_flag == 1)
        {
            g_tod_flag = 0;
            g_nl_flag = 0;
            uint64_t pps_ts = (uint64_t)g_pps_in_sec * 1000000000ULL + g_pps_in_nsec;
            uint64_t gtc_cnt = (uint64_t)g_latch_gtc_hicnt << 32 | g_latch_gtc_lwcnt;
            if (g_tod_ts && g_tod_cnt && pps_ts && gtc_cnt && gtc_cnt == g_tod_cnt)
            {
                m_master_offset = pps_ts - g_tod_ts - m_utc_offset;
                uint8_t servo_status = 0;
                // 获取servo状态及频率偏差
                double ppb = OffsetSample(m_master_offset, pps_ts, servo_status);
                m_freq_offset = ppb;
                if (servo_status >= 2)
                    m_is_synchronized = true;
                else
                    m_is_synchronized = false;
                int res = -1;
                if (servo_status == 1)
                {
                    res = ClockAdjSetFreq(m_clock_id, -ppb);
                    if (res != 0)
                    {
                        std::cout << "ClockAdjSetFreq failed ppb: " << ppb << std::endl;
                    }
                    res = ClockAdjStep(m_clock_id, -m_master_offset);
                    if (res != 0)
                    {
                        std::cout << "ClockAdjStep failed" << std::endl;
                    }
                }
                else if (servo_status == 2)
                {
                    res = ClockAdjSetFreq(m_clock_id, -ppb);
                    if (res != 0)
                    {
                        std::cout << "ClockAdjSetFreq failed ppb: " << ppb << std::endl;
                    }
                }
                std::cout << "offset: " << m_master_offset << " ppb: " << -ppb << " S" << (uint32_t)servo_status << std::endl;
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

PSDK_PLUGIN_EXPORT_C
auto GetPluginFactory() -> IPluginFactory *
{
    static PluginFactory pinfo = []
    {
        auto p = PluginFactory("todpps", "0.1-alpha");
        p.registerClass<TodppsInterface>("TodppsInterface");
        return p;
    }();
    return &pinfo;
}

struct _DLLInit
{
    _DLLInit()
    {
        std::cout << "Shared library todpps plugin loaded OK" << std::endl;
    }
    ~_DLLInit()
    {
        std::cout << "Shared library todpps plugin unloaded OK" << std::endl;
    }
} dll_init;

} // namespace protocol
} // namespace timesync
} // namespace hanhai
