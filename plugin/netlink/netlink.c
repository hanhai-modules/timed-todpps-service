/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef __cplusplus
extern "C"
{
#endif

#include "netlink.h"
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <linux/netlink.h>
#include <linux/genetlink.h>
#include <fcntl.h>

#define GTC_GENL_NAME "BST_GTC_GENL"
#define GTC_GENL_VERSION 1
#define MAX_MSG_SIZE 256
#define GENLMSG_DATA(glh) ((void *)(NLMSG_DATA(glh) + GENL_HDRLEN))
#define GENLMSG_PAYLOAD(glh) (NLMSG_PAYLOAD(glh, 0) - GENL_HDRLEN)
#define NLA_DATA(na) ((void *)((char *)(na) + NLA_HDRLEN))

/* commands */
enum
{
    GTC_CMD_UNSPEC,
    GTC_CMD_SYNC_INFO,
    GTC_CMD_USER_INFO,
    __GTC_CMD_MAX,
};
#define GTC_CMD_MAX (__GTC_CMD_MAX - 1)

/* attribute */
enum
{
    GTC_ATTR_UNSPEC,
    GTC_ATTR_SYNC_INFO,
    GTC_ATTR_USER_INFO,
    __GTC_ATTR_MAX,
};
#define GTC_ATTR_MAX (__GTC_ATTR_MAX - 1)

typedef struct
{
    struct nlmsghdr nlh;
    struct genlmsghdr gnlh;
    char data[MAX_MSG_SIZE];
} gtc_msg_t;

typedef struct
{
    unsigned int latch_gtc_hicnt;
    unsigned int latch_gtc_lwcnt;
    long long phc_utc_sec;
    long phc_utc_nsec;
    unsigned int gtc_hicnt;
    unsigned int gtc_lwcnt;
} time_sync_parm_t;

typedef struct
{
    char flag;
    uint32_t data;
} user_msg_t;

static int32_t g_sock_fd = -1;
static int32_t g_family_id = -1;
static int32_t running = 0;

uint32_t g_latch_gtc_hicnt = 0;
uint32_t g_latch_gtc_lwcnt = 0;
uint32_t g_pps_in_sec = 0;
uint32_t g_pps_in_nsec = 0;
int32_t g_nl_flag = 0;

/**
 * send data to kernel by generic netlink
 *
 * @g_sock_fd: client socket
 * @g_family_id: family id
 * @nlmsg_pid: client pid
 * @genl_cmd: cmd type
 * @genl_version: genl version
 * @nla_type: netlink attr type
 * @nla_data: data
 * @nla_len: data len
 *
 * return:
 * 0: success; -1: fail
 */
static int32_t genl_send_msg(
    int32_t g_sock_fd,
    int16_t g_family_id,
    int32_t nlmsg_pid,
    int8_t genl_cmd,
    int8_t genl_version,
    int16_t nla_type,
    void *nla_data,
    int32_t nla_len)
{
    char *buf;
    gtc_msg_t msg;
    struct nlattr *na;
    int32_t ret = -1, buflen;
    struct sockaddr_nl dst_addr;

    /* family id 0 is reserve for ctrl */
    if (g_family_id == 0)
    {
        printf("%s family id is invalid\n", __func__);
        return -1;
    }

    /* construct netlink header */
    msg.nlh.nlmsg_len = NLMSG_LENGTH(GENL_HDRLEN);
    msg.nlh.nlmsg_type = g_family_id;
    msg.nlh.nlmsg_flags = NLM_F_REQUEST; // request msg
    msg.nlh.nlmsg_seq = 0;
    msg.nlh.nlmsg_pid = nlmsg_pid;

    /* construct genl netlink header */
    msg.gnlh.cmd = genl_cmd;
    msg.gnlh.version = genl_version;
    na = (struct nlattr *)GENLMSG_DATA(&msg);
    na->nla_type = nla_type;
    na->nla_len = nla_len + 1 + NLA_HDRLEN;
    memcpy(NLA_DATA(na), nla_data, nla_len);
    msg.nlh.nlmsg_len += NLMSG_ALIGN(na->nla_len);
    buf = (char *)&msg;
    buflen = msg.nlh.nlmsg_len;

    /* construct dest addr */
    memset(&dst_addr, 0, sizeof(dst_addr));
    dst_addr.nl_family = AF_NETLINK;
    dst_addr.nl_pid = 0;    // dest pid is 0 (to kernel is 0)
    dst_addr.nl_groups = 0; // unicast

    /* send msg to kernel */
    while ((ret = sendto(g_sock_fd, buf, buflen, 0, (struct sockaddr *)&dst_addr, sizeof(dst_addr))) < buflen)
    {
        if (ret > 0)
        {
            buf += ret;
            buflen -= ret;
        }
        else if (errno != EAGAIN)
        {
            return -1;
        }
    }

    return 0;
}

/**
 * get gtc family id
 *
 * @g_sock_fd: client socket
 * @family_name: family name registered on kernel
 *
 * return:
 * 0: success; -1: fail
 */
static int32_t get_gtc_family_id(int32_t g_sock_fd, char *family_name, uint32_t *event_group)
{
    gtc_msg_t ans;
    struct nlattr *na;
    struct nlattr *grps;
    struct nlattr *grp;
    int32_t id, ret, rep_len, len;

    ret = genl_send_msg(g_sock_fd, GENL_ID_CTRL, 0, CTRL_CMD_GETFAMILY, 1,
                        CTRL_ATTR_FAMILY_NAME, (void *)family_name,
                        strlen(family_name) + 1);
    if (ret)
    {
        printf("%s send genl msg fail\n", __func__);
        return -1;
    }

    rep_len = recv(g_sock_fd, &ans, sizeof(ans), 0);
    if (rep_len < 0)
    {
        printf("%s get family id fail\n", __func__);
        return -1;
    }

    if (ans.nlh.nlmsg_type == NLMSG_ERROR || !NLMSG_OK((&ans.nlh), rep_len))
    {
        printf("%s get kernel msg is invalud\n", __func__);
        return -1;
    }

    na = (struct nlattr *)GENLMSG_DATA(&ans);

    len = 0;
    rep_len = GENLMSG_PAYLOAD(&ans.nlh);
    na = (struct nlattr *)GENLMSG_DATA(&ans);
    while (len < rep_len)
    {
        len += NLA_ALIGN(na->nla_len);
        if (na->nla_type == CTRL_ATTR_FAMILY_ID)
        {
            id = *(uint16_t *)NLA_DATA(na);
        }
        else if (na->nla_type == CTRL_ATTR_MCAST_GROUPS)
        {
            struct nlattr *nested_na;
            struct nlattr *group_na;
            int32_t group_attr_len;
            int32_t group_attr;

            nested_na = (struct nlattr *)((char *)na + NLA_HDRLEN);
            group_na = (struct nlattr *)((char *)nested_na + NLA_HDRLEN);
            group_attr_len = 0;

            for (group_attr = CTRL_ATTR_MCAST_GRP_UNSPEC; group_attr < CTRL_ATTR_MCAST_GRP_MAX; group_attr++)
            {
                if (group_na->nla_type == CTRL_ATTR_MCAST_GRP_ID)
                {
                    *event_group = *(uint32_t *)((char *)group_na + NLA_HDRLEN);
                    break;
                }

                group_attr_len += NLA_ALIGN(group_na->nla_len) + NLA_HDRLEN;
                if (group_attr_len >= nested_na->nla_len)
                    break;

                group_na = (struct nlattr *)((char *)group_na + NLA_ALIGN(group_na->nla_len));
            }
        }
        na = (struct nlattr *)(GENLMSG_DATA(&ans) + len);
    }

    return id;
}

int32_t Netlink_Init(void)
{
    int32_t ret = 0;
    uint32_t group = 0xf;
    struct sockaddr_nl src_addr;
    user_msg_t umsg;

    /* create a socket */
    g_sock_fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_GENERIC);
    if (g_sock_fd < 0)
    {
        printf("Error creating gtc user socket: %s\n", strerror(errno));
        return -1;
    }

    g_family_id = get_gtc_family_id(g_sock_fd, GTC_GENL_NAME, &group);
    if (g_family_id == 0)
    {
        printf("Error getting gtc family id\n");
        close(g_sock_fd);
        return -1;
    }
    printf("Gtc family id = %d, group id = %d\n", g_family_id, group);

    /* prepare bind parms */
    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family = AF_NETLINK;
    src_addr.nl_pid = getpid();
    src_addr.nl_groups = group;

    /* action bind */
    ret = bind(g_sock_fd, (struct sockaddr *)&src_addr, sizeof(src_addr));
    if (ret < 0)
    {
        printf("Error binding gtc user socket: %s\n", strerror(errno));
        close(g_sock_fd);
        return -1;
    }

    /* Set socket option */
    ret = setsockopt(g_sock_fd, SOL_NETLINK, NETLINK_ADD_MEMBERSHIP, &group, sizeof(group));
    if (ret < 0)
    {
        printf("Error joining the gtc multicast group\n");
        close(g_sock_fd);
        return -1;
    }

    /* send pid to gtc */
    umsg.flag = 1; // 1: pid flag, 0: normal data
    umsg.data = src_addr.nl_pid;

    ret = genl_send_msg(g_sock_fd, g_family_id, src_addr.nl_pid, GTC_CMD_USER_INFO, 1, GTC_ATTR_USER_INFO, &umsg, sizeof(umsg));
    if (ret != 0)
    {
        printf("Error sending genl msg\n");
        close(g_sock_fd);
        return -1;
    }

    /* set to non-block mode */
    ret = fcntl(g_sock_fd, F_SETFL, O_NONBLOCK);
    if (ret != 0)
    {
        printf("Error setting non-block mode\n");
        close(g_sock_fd);
        return -1;
    }

    return 0;
}

void Netlink_Start(void)
{
    struct nlmsgerr *err;
    gtc_msg_t gtc_msg;
    time_sync_parm_t *sync_info;
    struct nlattr *nla;
    struct pollfd pfd;
    int32_t ret = 0, len = 0;

    pfd.fd = g_sock_fd;
    pfd.events = POLLIN; // listen readable events
    pfd.revents = 0;

    running = 1;

    while (running)
    {
        /* use poll to wait for events without occupy the CPU, -1 means infinite waiting, now wait 1000 ms */
        ret = poll(&pfd, 1, 1000);
        if (ret < 0)
        {
            printf("Error gtc polling\n");
            break;
        }
        else if (ret == 0)
        {
            continue; // timeout handle
        }

        if (pfd.revents & POLLIN)
        {
            len = recv(g_sock_fd, &gtc_msg, sizeof(gtc_msg), 0);
            if (len < 0)
            {
                if (errno == EAGAIN || errno == EWOULDBLOCK)
                {
                    /* no data to read, continue polling */
                    continue;
                }
                printf("Error receiving gtc msg\n");
                break;
            }
            else if (len == 0)
            {
                printf("Netlink connection closed\n");
                break;
            }

            if (gtc_msg.nlh.nlmsg_type == NLMSG_ERROR)
            {
                err = (struct nlmsgerr *)NLMSG_DATA(&gtc_msg);
                printf("Error getting nlmsg type, ERR_NUM = %d - %s\n", err->error, strerror(-err->error));
                break;
            }

            if (gtc_msg.nlh.nlmsg_type == g_family_id && gtc_msg.gnlh.cmd == GTC_CMD_SYNC_INFO)
            {
                nla = (struct nlattr *)GENLMSG_DATA(&gtc_msg);
                sync_info = (time_sync_parm_t *)NLA_DATA(nla);
                g_latch_gtc_hicnt = sync_info->latch_gtc_hicnt;
                g_latch_gtc_lwcnt = sync_info->latch_gtc_lwcnt;
                g_pps_in_sec = sync_info->phc_utc_sec;
                g_pps_in_nsec = sync_info->phc_utc_nsec;
                g_nl_flag = 1;
            }
        }
    }

    running = 0;
}

void Netlink_Stop(void)
{
    running = 0;
}

void Netlink_Destroy(void)
{
    close(g_sock_fd);
}

#ifdef __cplusplus
}
#endif
