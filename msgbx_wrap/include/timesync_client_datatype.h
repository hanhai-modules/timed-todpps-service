/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TSYNC_CLIENT_DATATYPE_H
#define TSYNC_CLIENT_DATATYPE_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

typedef enum
{
	MASTER_ONLY = 0,
	SLAVE_ONLY = 1,
	BC = 2,
	NONE = 3
} RoleType_t;

typedef enum
{
	ADAS = 0,
	SWITCH = 1,
	REALTIME = 2,
	SAFETY = 3,
	DB = 4,
	IVI = 5
} Domain;

typedef struct
{
	uint32_t todpps_mode;
	uint32_t cansync_mode;
	uint32_t gptp_mode;
	uint32_t ptp_mode;
} TimeSyncConfig;

#ifdef __cplusplus
}
#endif

#endif // TSYNC_CLIENT_DATATYPE_H
