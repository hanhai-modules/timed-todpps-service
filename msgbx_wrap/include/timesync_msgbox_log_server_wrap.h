/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TIMESYNC_MSGBOX_LOG_SERVER_WRAP_H
#define TIMESYNC_MSGBOX_LOG_SERVER_WRAP_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdint.h>

typedef void (*log_func_t)(const uint32_t domain_id, const uint32_t status, const char *log_msg);

/**
 * @brief 初始化msgbox server
 * @param pid server节点的pid属性
 * @param fid server节点的fid属性
 * @param sid server节点的sid属性
 * @return 成功返回true，失败返回false
 */
bool timesync_msgbox_log_server_init(int32_t pid, int32_t fid, int32_t sid);


/**
 * @brief 注册日志记录回调函数，即timesync_log method的回调函数
 */
void timesync_msgbox_server_reg_log_func(log_func_t func);

/**
 * @brief 注销msgbox server
 */
void timesync_msgbox_log_server_destroy(void);

#ifdef __cplusplus
}
#endif

#endif // TIMESYNC_MSGBOX_LOG_SERVER_WRAP_H
