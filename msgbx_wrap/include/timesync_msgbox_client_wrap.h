/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TIMESYNC_MSGBOX_CLIENT_WRAP_H
#define TIMESYNC_MSGBOX_CLIENT_WRAP_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include "timesync_client_datatype.h"

/**
 * @brief 初始化msgbox client
 * @param pid client节点的pid属性
 * @param fid client节点的fid属性
 * @param sid client节点的sid属性
 * @return 成功返回true，失败返回false
 */
bool timesync_msgbox_client_init(int32_t pid, int32_t fid, int32_t sid);

/**
 * @brief 发送同步模式设置请求
 * @param domain 域标识符
 * @param tsync_config 模式配置字段
 * @param result_status 结果状态值，属于out参数
 * @return 成功返回true，失败返回false
 */
bool timesync_msgbox_client_send_mode_info(Domain domain, TimeSyncConfig *tsync_config, uint32_t *result_status);

/**
 * @brief 发送时间同步请求
 * @param domain 域标识符
 * @param sec pps发送时刻的秒部分
 * @param nsec pps发送时刻的纳秒部分
 * @param hicnt gtc计数快照的高32位
 * @param lwcnt gtc计数快照的低32位
 * @return 成功返回true，失败返回false
 */
bool timesync_msgbox_client_send_tod(Domain domain, const uint32_t sec, const uint32_t nsec, const uint32_t hicnt, const uint32_t lwcnt);

/**
 * @brief 注销msgbox client
 */
void timesync_msgbox_client_destroy(void);

#ifdef __cplusplus
}
#endif

#endif // TIMESYNC_MSGBOX_CLIENT_WRAP_H
