/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef __cplusplus
extern "C"
{
#endif

#include "timesync_msgbox_client_wrap.h"
#include "TimeSyncAdasClient.h"
#include <stdio.h>

#define GTC_FREQ_MHZ 200

static TimeSyncAdasClient_t *g_msgbox_client = NULL;
static TimeSyncAdasClient_data_t g_client_data = {0};

bool timesync_msgbox_client_init(int32_t pid, int32_t fid, int32_t sid)
{
	int32_t ret = 0;

	if (g_msgbox_client != NULL)
		return true;

	g_client_data.com_data.pid = pid;
	g_client_data.com_data.fid = fid;
	g_client_data.com_data.sid = sid;
	g_msgbox_client = TimeSyncAdasClient_init(&g_client_data);
	if (g_msgbox_client == NULL)
	{
		printf("Error initializing msgbox client\n");
		return false;
	}

	ret = g_msgbox_client->start();
	if (ret != 0)
	{
		printf("Error starting msgbox client, ret = %d\n", ret);
		TimeSyncAdasClient_destroy();
		g_msgbox_client = NULL;
		return false;
	}

	return true;
}

bool timesync_msgbox_client_send_mode_info(Domain domain, TimeSyncConfig *tsyn_cfg, uint32_t *result_status)
{
	int32_t error = 0, ret = 0;

	if (g_msgbox_client == NULL)
	{
		printf("Unavailable msgbox client\n");
		return false;
	}

	switch (domain)
	{
	case SWITCH:
		ret = g_msgbox_client->switch_client.switch_timesync_mode_set_sync((struct _switch_TimeSyncConfig_t *)tsyn_cfg,
						result_status, (enum _switch_ErrorEnum_t *)&error, 1000, NULL);
		break;
	case REALTIME:
		ret = g_msgbox_client->realtime_client.realtime_timesync_mode_set_sync((struct _realtime_TimeSyncConfig_t *)tsyn_cfg,
						result_status, (enum _realtime_ErrorEnum_t *)&error, 1000, NULL);
		break;
	default:
		printf("Invalid domain\n");
		return false;
	}

	if (ret != 0)
	{
		printf("Error sending mode config info, ret = %d\n", ret);
		return false;
	}

	return true;
}

bool timesync_msgbox_client_send_tod(Domain domain, const uint32_t sec, const uint32_t nsec, const uint32_t hicnt, const uint32_t lwcnt)
{
	int32_t ret = 0;

	if (g_msgbox_client == NULL)
	{
		printf("Unavailable msgbox client\n");
		return false;
	}

	switch (domain)
	{
	case SWITCH:
		ret = g_msgbox_client->switch_client.switch_timesync_fire_and_forget(sec, nsec, hicnt, lwcnt);
		break;
	case REALTIME:
		ret = g_msgbox_client->realtime_client.realtime_timesync_fire_and_forget(sec, nsec, hicnt, lwcnt);
		break;
	case DB:
		ret = g_msgbox_client->db_client.db_timesync_fire_and_forget(sec, nsec, hicnt, lwcnt, GTC_FREQ_MHZ);
		break;
	case IVI:
		ret = g_msgbox_client->ivi_client.ivi_timesync_fire_and_forget(sec, nsec, hicnt, lwcnt, GTC_FREQ_MHZ);
		break;
	default:
		printf("Invalid domain\n");
		return false;
	}

	if (ret != 0)
	{
		printf("Error sending tod , ret = %d\n", ret);
		return false;
	}

	return true;
}

void timesync_msgbox_client_destroy(void)
{
	int32_t ret = 0;

    if (g_msgbox_client != NULL)
    {
        ret = g_msgbox_client->stop();
        if (ret != 0)
            printf("Error stopping msgbox client, ret = %d\n", ret);

        TimeSyncAdasClient_destroy();
        g_msgbox_client = NULL;
    }
}

#ifdef __cplusplus
}
#endif
