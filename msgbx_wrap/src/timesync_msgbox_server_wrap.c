/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef __cplusplus
extern "C"
{
#endif

#include "TimeSyncAdasServer.h"
#include "timesync_msgbox_server_wrap.h"
#include <stdio.h>

static TimeSyncAdasServer_t *g_msgbox_server = NULL;
static TimeSyncAdasServer_data_t g_server_data = {0};
static timesync_func_t g_tsyn_func = NULL;

static void on_tod_received(
    const uint32_t sec,
    const uint32_t nsec,
    const uint32_t hicnt,
    const uint32_t lwcnt,
    const ext_info_t *info)
{
    if (g_tsyn_func != NULL)
        g_tsyn_func(sec, nsec, hicnt, lwcnt);
}

bool timesync_msgbox_server_init(int32_t pid, int32_t fid, int32_t sid)
{
    int32_t ret = 0;

    if (g_msgbox_server != NULL)
        return true;

    g_server_data.com_data.pid = pid;
    g_server_data.com_data.fid = fid;
    g_server_data.com_data.sid = sid;
    g_msgbox_server = TimeSyncAdasServer_init(&g_server_data);
    if (g_msgbox_server == NULL)
    {
        printf("Error initializing msgbox server\n");
        return false;
    }

    ret = g_msgbox_server->adas_server.register_adas_timesync(&on_tod_received);
    if (ret != 0)
    {
        printf("Error registering timesync callback, ret = %d\n", ret);
        TimeSyncAdasServer_destroy();
        g_msgbox_server = NULL;
        return false;
    }

    ret = g_msgbox_server->start();
    if (ret != 0)
    {
        printf("Error starting msgbox server, ret = %d\n", ret);
        TimeSyncAdasServer_destroy();
        g_msgbox_server = NULL;
        return false;
    }

    return true;
}

void timesync_msgbox_server_reg_timesync_func(timesync_func_t func)
{
    g_tsyn_func = func;
}

void timesync_msgbox_server_destroy(void)
{
    int32_t ret = 0;

    if (g_msgbox_server != NULL)
    {
        ret = g_msgbox_server->stop();
        if (ret != 0)
            printf("Error stopping msgbox server, ret = %d\n", ret);

        TimeSyncAdasServer_destroy();
        g_msgbox_server = NULL;
    }
}

#ifdef __cplusplus
}
#endif
