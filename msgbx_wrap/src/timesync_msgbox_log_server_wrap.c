/*
 * Copyright (c) 2024 Black Sesame Technologies
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef __cplusplus
extern "C"
{
#endif

#include "TimeSyncAdasLogServer.h"
#include "timesync_msgbox_log_server_wrap.h"
#include <stdio.h>

static TimeSyncAdasLogServer_t *g_msgbox_log_server = NULL;
static TimeSyncAdasLogServer_data_t g_log_server_data = {0};
static log_func_t g_log_func = NULL;

static void on_log_received(
    const uint32_t domain_id,
    const uint32_t status,
    const char *log_msg,
    const ext_info_t *info)
{
    if (g_log_func != NULL)
        g_log_func(domain_id, status, log_msg);
}

bool timesync_msgbox_log_server_init(int32_t pid, int32_t fid, int32_t sid)
{
    int32_t ret = 0;

    if (g_msgbox_log_server != NULL)
        return true;

    g_log_server_data.com_data.pid = pid;
    g_log_server_data.com_data.fid = fid;
    g_log_server_data.com_data.sid = sid;
    g_msgbox_log_server = TimeSyncAdasLogServer_init(&g_log_server_data);
    if (g_msgbox_log_server == NULL)
    {
        printf("Error initializing msgbox log server\n");
        return false;
    }

    ret = g_msgbox_log_server->adas_log_server.register_timesync_log(&on_log_received);
    if (ret != 0)
    {
        printf("Error registering log callback, ret = %d\n", ret);
        TimeSyncAdasLogServer_destroy();
        g_msgbox_log_server = NULL;
        return false;
    }

    ret = g_msgbox_log_server->start();
    if (ret != 0)
    {
        printf("Error starting msgbox server, ret = %d\n", ret);
        TimeSyncAdasLogServer_destroy();
        g_msgbox_log_server = NULL;
        return false;
    }

    return true;
}


void timesync_msgbox_server_reg_log_func(log_func_t func)
{
    g_log_func = func;
}

void timesync_msgbox_log_server_destroy(void)
{
    int32_t ret = 0;

    if (g_msgbox_log_server != NULL)
    {
        ret = g_msgbox_log_server->stop();
        if (ret != 0)
            printf("Error stopping msgbox log server, ret = %d\n", ret);

        TimeSyncAdasLogServer_destroy();
        g_msgbox_log_server = NULL;
    }
}

#ifdef __cplusplus
}
#endif
